import React, { useEffect, useState } from "react";
import { Alert, BackHandler, Dimensions, StyleSheet, StatusBar, } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "./views/LoginScreen";
import LoggedIn from "./views/LoggedIn";

const Stack = createStackNavigator();
const { width, height } = Dimensions.get("window");

export default function App() {
  StatusBar.setBarStyle("dark-content", true);

  useEffect(() => {
    const backAction = () => {
      Alert.alert("Wait! Are you sure you wanna leave?", [
        {
          text: "Whoa! No way, this is too much fun!",
          onPress: () => null,
          style: "cancel",
        },
        { text: "Get me outta here!", onPress: () => BackHandler.exitApp() },
      ]);
      return true;
    };
    const backHandler = BackHandler.addEventListener("hardwareBackPress", backAction);
    return () => backHandler.remove();
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          gestureEnabled: false,
        }}>
        <Stack.Screen name='Login'>{(props) => <LoginScreen {...props} />}</Stack.Screen>

        <Stack.Screen name='LoggedIn'>{(props) => <LoggedIn {...props} />}</Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    backgroundColor: "purple",
    alignItems: "center",
    justifyContent: "center",
  },
});
