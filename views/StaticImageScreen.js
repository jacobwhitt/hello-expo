import React, { useState } from 'react'
import { Image, PixelRatio, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

const StaticImageScreen = (props) => {
  const [active, setActive] = useState(true)

  //! BAD
  let badIcon = active ? 'active' : 'inactive'

  //* GOOD
  let goodIcon = active ? require('../assets/active.png') : require('../assets/inactive.png')

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Using the state, you can toggle between images to give valuable visual feedback to your users</Text>

      <View style={[styles.box, { borderColor: 'red' }]}>
        <Text style={styles.subtitle}>BAD!</Text>
        <Text style={styles.text}>badIcon = active ? 'active' : 'inactive'</Text>
        <Text style={styles.text}>Image source= require('./' + badIcon + '.png')</Text>
        <Text style={{ marginTop: 20 }}>This not only doesn't render, it throws an error and will crash the entire app</Text>
      </View>

      <View style={[styles.box, { borderColor: 'lime' }]}>
        <Text style={styles.subtitle}>GOOD!</Text>
        <Text style={styles.text}>let goodIcon = active ? require('../../assets/active.png') : require('../../assets/inactive.png')</Text>
        <Text style={styles.text}> Image source = goodIcon</Text>
        <Text>(don't forget the curly brackets around the variable!)</Text>
        <Image style={styles.icon} source={goodIcon} />

        <TouchableOpacity
          style={styles.greenBtn}
          onPress={() => {
            setActive(!active)
          }}>
          <Text style={styles.btnText}>Test Me!</Text>
        </TouchableOpacity>
      </View>

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate('HomeContainer')
        }}>
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </View>
  )
}

export default StaticImageScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  back: {
    position: "absolute",
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: "#9a031e",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    fontStyle: "italic",
  },
  box: {
    flexDirection: "column",
    margin: 10,
    padding: 10,
    borderWidth: 3,
    borderColor: "#000",
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  icon: {
    width: 80,
    height: 80,
  },
  text: {
    fontSize: 18,
    textAlign: "center",
  },
  subtitle: {
    fontSize: 20,
    textAlign: "center",
  },
  greenBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: "#90be6d",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
});
