import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
const Stack = createStackNavigator()

import HomeContainer from './HomeContainer'
import ContactsScreen from './ContactsScreen'
import DetailScreen from './DetailScreen'

import PlatformCheck from './PlatformCheck'
import AndroidComponent from './Android/AndroidComponent'
import IosComponent from './iOS/IosComponent'
import RefreshScreen from './RefreshScreen'
import PressableScreen from './PressableScreen'
import ModalScreen from './ModalScreen'
import ExpandScreen from './ExpandScreen'
import ShareScreen from './ShareScreen'
import StaticImageScreen from './StaticImageScreen'
import PixelRatioScreen from './PixelRatioScreen'
import BatteryScreen from './BatteryScreen'
// import OrientationScreen from './OrientationScreen'
import ImagePickerScreen from './ImagePickerScreen'
import NotificationsScreen from './NotificationsScreen'

const Home = (props) => {
  return (
    <Stack.Navigator
      headerMode='none'
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}>
      <Stack.Screen name='HomeContainer'>{(props) => <HomeContainer {...props} />}</Stack.Screen>

      <Stack.Screen name='Contacts'>{(props) => <ContactsScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Details'>{(props) => <DetailScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='PlatformCheck'>{(props) => <PlatformCheck {...props} />}</Stack.Screen>

      <Stack.Screen name='AndroidComponent'>{(props) => <AndroidComponent {...props} />}</Stack.Screen>

      <Stack.Screen name='IosComponent'>{(props) => <IosComponent {...props} />}</Stack.Screen>

      <Stack.Screen name='Pressable'>{(props) => <PressableScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Modal'>{(props) => <ModalScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Refresh'>{(props) => <RefreshScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Expand'>{(props) => <ExpandScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Share'>{(props) => <ShareScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='StaticImage'>{(props) => <StaticImageScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Pixel'>{(props) => <PixelRatioScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Battery'>{(props) => <BatteryScreen {...props} />}</Stack.Screen>

      {/* <Stack.Screen name='Orientation'>{(props) => <OrientationScreen {...props} />}</Stack.Screen> */}

      <Stack.Screen name='ImagePicker'>{(props) => <ImagePickerScreen {...props} />}</Stack.Screen>

      <Stack.Screen name='Notifications'>{(props) => <NotificationsScreen {...props} />}</Stack.Screen>
    </Stack.Navigator>
  )
}

export default Home
