import Constants from 'expo-constants'
import * as Notifications from 'expo-notifications'
import React, { useState, useEffect, useRef } from "react";
import {
  Text,
  View,
  Platform,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const schedulePushNotification = async () => {
  await Notifications.scheduleNotificationAsync({
    content: {
      title: "You've got mail! 📬",
      body: "Here is the notification body",
      data: { data: random() },
    },
    trigger: { seconds: 2 },
  });
};

const registerForPushNotificationsAsync = async () => {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      alert("Failed to get push token for push notification!");
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    alert("Must use physical device for Push Notifications");
  }

  if (Platform.OS === "android") {
    Notifications.setNotificationChannelAsync("default", {
      name: "default",
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: "#FF231F7C",
    });
  }

  return token;
};

const random = () => {
  return Math.floor(Math.random() * (9999 - 1000 + 1) + 1000);
};

const NotificationsScreen = (props) => {
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    registerForPushNotificationsAsync().then((token) =>
      setExpoPushToken(token)
    );

    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        console.log(response);
      });

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Your expo push token: {expoPushToken}</Text>

      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text style={styles.text}>
          Title: {notification && notification.request.content.title}{" "}
        </Text>

        <Text style={styles.text}>
          Body: {notification && notification.request.content.body}
        </Text>

        <Text style={styles.text}>
          Data:{" "}
          {notification && JSON.stringify(notification.request.content.data)}
        </Text>
      </View>

      <TouchableOpacity
        style={styles.tealBtn}
        onPress={async () => {
          await schedulePushNotification();
        }}
      >
        <Text style={styles.btnText}>Press to schedule a notification</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.back}
        onPress={async () => {
          props.navigation.navigate("HomeContainer");
        }}
      >
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </View>
  );
};

export default NotificationsScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'purple',
  },
  tealBtn: {
    width: 180,
    height: 80,
    marginTop: 20,
    backgroundColor: '#43aa8b',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  uploadBtn: {
    fontSize: 30,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  image: {
    marginTop: 20,
    width: 300,
    height: 300,
    borderRadius: 15,
    resizeMode: 'contain',
  },
  text: {
    fontSize: 20,
    marginTop: 20,
    textAlign: 'center',
  },
})
