import React, { useState } from 'react'
import { ScrollView, RefreshControl, StyleSheet, Text, SafeAreaView, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout)
  })
}

const RefreshScreen = (props) => {
  const [refreshing, setRefreshing] = useState(false)
  const [showSecret, setShowSecret] = useState(false)

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)

    wait(2000).then(() => {
      setRefreshing(false)
      console.log('Refrescado!')
      setShowSecret(true)
    })
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.scrollWidth}
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl
            title="How refreshing!"
            titleColor="#000"
            tintColor="#000"
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      >
        {showSecret ? (
          <>
            <Text style={styles.text}>Click the button to go back</Text>
            <TouchableOpacity
              style={styles.secret}
              onPress={() => {
                setShowSecret(false);
                props.navigation.navigate("HomeContainer");
              }}
            >
              <Text style={styles.btnText}>A Refreshing Secret!</Text>
            </TouchableOpacity>
          </>
        ) : (
          <Text style={styles.text}>Tira abajo para abrir RefreshControl</Text>
        )}
      </ScrollView>

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate("HomeContainer");
        }}
      >
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

export default RefreshScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 24,
  },
  scrollView: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  secret: {
    width: 160,
    height: 80,
    marginTop: 20,
    backgroundColor: '#5856d6',
    borderRadius: 5,
    justifyContent: 'center',
  },
  btnText: {
    textAlign: 'center',
    fontSize: 24,
    color: '#fff',
  },
  scrollWidth: {
    width: '100%',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
})
