import React, { useState, useEffect } from 'react'
import { Button, Image, View, Platform, StyleSheet, TouchableOpacity, Text } from 'react-native'
import * as ImagePicker from 'expo-image-picker'

const ImagePickerDemo = (props) => {
  const [image, setImage] = useState(null)

  useEffect(() => {
    const getPermission = async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync()
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!')
        }
      }
    }
  }, [])

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
    })

    console.log('image chosen successfully: ', result)

    if (!result.cancelled) {
      setImage(result.uri)
    }
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.greenBtn} onPress={pickImage}>
        <Text style={styles.uploadBtn}>Upload Image</Text>
      </TouchableOpacity>

      {image && <Image source={{ uri: image }} style={styles.image} />}

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate('HomeContainer')
        }}>
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </View>
  )
}

export default ImagePickerDemo

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  greenBtn: {
    width: 180,
    height: 80,
    marginTop: 20,
    backgroundColor: '#90be6d',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  back: {
    position: 'absolute',
    top: 30,
    left: 20,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  uploadBtn: {
    fontSize: 30,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  image: {
    marginTop: 20,
    width: 300,
    height: 300,
    borderRadius: 15,
    resizeMode: 'contain',
  },
})
