import React from 'react'
import { DrawerContentScrollView, DrawerItemList, DrawerItem, createDrawerNavigator } from '@react-navigation/drawer'

import Home from './Home'
import SettingsScreen from './SettingsScreen'

const Drawer = createDrawerNavigator()

const LoggedIn = (props) => {
  return (
    <Drawer.Navigator
      initialRouteName='Home'
      drawerContent={(props) => {
        return (
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem label='Logout' onPress={() => props.navigation.navigate('Login')} />
          </DrawerContentScrollView>
        )
      }}>
      <Drawer.Screen name='Home'>{(props) => <Home {...props} />}</Drawer.Screen>

      <Drawer.Screen name='Settings'>{(props) => <SettingsScreen {...props} />}</Drawer.Screen>
    </Drawer.Navigator>
  )
}
export default LoggedIn
