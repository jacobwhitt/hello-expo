import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const DetailScreen = ({ route, navigation }) => {
  const { id, name, surname, email, phone } = route.params

  return (
    <View style={styles.container}>
      <View style={styles.contactInfo}>
        <Text style={styles.text}>ID: {id}</Text>
        <Text style={styles.text}>Name: {name}</Text>
        <Text style={styles.text}>Surname: {surname}</Text>
        <Text style={styles.text}>Email: {email}</Text>
        <Text style={styles.text}>Phone: {phone}</Text>
      </View>

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          navigation.navigate('Contacts')
        }}>
        <Text style={styles.btnText}>Contacts</Text>
      </TouchableOpacity>
    </View>
  )
}

export default DetailScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contactInfo: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  text: {
    fontSize: 20,
    lineHeight: 60,
  },
})
