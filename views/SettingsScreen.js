import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const SettingsScreen = ({ props, navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>This is a place holder for the Settings Screen</Text>

      <TouchableOpacity
        onPress={() => {
          navigation.navigate('HomeContainer')
        }}
        style={styles.logoutBtn}>
        <Text style={styles.btnText}>Back</Text>
      </TouchableOpacity>
    </View>
  )
}

export default SettingsScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoutBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    textAlign: 'center',
    fontSize: 24,
    color: '#fff',
  },
  text: {
    fontSize: 24,
  },
})
