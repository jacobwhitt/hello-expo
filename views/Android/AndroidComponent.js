import React from 'react'
import { DrawerContentScrollView, DrawerItemList, DrawerItem, createDrawerNavigator } from '@react-navigation/drawer'
const Drawer = createDrawerNavigator()

import AndroidWelcome from './AndroidWelcome'
import AndroidImage from './AndroidImage'
import AndroidRefresh from './AndroidRefresh'

const AndroidComponent = (props) => {
  return (
    <Drawer.Navigator
      initialRouteName='AndroidWelcome'
      drawerContent={(props) => {
        return (
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem label='Back' onPress={() => props.navigation.navigate('PlatformCheck')} />
          </DrawerContentScrollView>
        )
      }}>
      <Drawer.Screen name='Android Welcome'>{(props) => <AndroidWelcome {...props} />}</Drawer.Screen>

      <Drawer.Screen name='Android Image'>{(props) => <AndroidImage {...props} />}</Drawer.Screen>

      <Drawer.Screen name='Android Refresh'>{(props) => <AndroidRefresh {...props} />}</Drawer.Screen>
    </Drawer.Navigator>
  )
}
export default AndroidComponent
