import React, { useState } from 'react'
import { Share, View, TouchableOpacity, Text, StyleSheet, } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'

const ShareScreen = (props) => {
  const [myMessage, setMyMessage] = useState('')
  const [shareResult, setShareResult] = useState('')

  const onShare = async () => {
    try {
      const result = await Share.share({
        subject: 'A test shared via Reaect Native Expo', //ios
        message: myMessage,
        url: 'https://whitt.tech', //ios
        dialogTitle: 'Una prueba de React Native Expo', //android
      })
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          //? shared with activity type of result.activityType
          console.log('shared as result.activityType: ', result)
        } else {
          //* shared
          console.log('shared: ', result)
        }
      } else if (result.action === Share.dismissedAction) {
        //! dismissed
        console.log('dismissed')
      }
    } catch (error) {
      console.log('ERROR! ', error.message)
      alert(error.message)
    }
  }

  const expr = shareResult.activityType
  switch (expr) {
    case 'com.apple.UIKit.activity.Mail':
      console.log('User successfully shared via the Mail app')
      break
    case 'com.apple.UIKit.activity.Message':
      console.log('User successfully shared via the Message app')
      break
    case 'net.whatsapp.WhatsApp.ShareExtension':
      console.log('User successfully shared via WhatsApp')
      break
    default:
    // console.log(`Sorry, we are out of ${expr}.`)
  }

  const onChangeText = (event) => {
    setMyMessage(event.nativeEvent.text)
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Type a message and click share!</Text>

      <TextInput style={styles.input} allowFontScaling autoCompleteType='off' autoCapitalize='sentences' clearTextOnFocus placeholderTextColor='#000' placeholder='Enter some text...' keyboardAppearance='dark' onSubmitEditing={onChangeText} returnKeyType='done' />

      {myMessage ? <Text style={[styles.text, { marginTop: 20 }]}>Stored message: {myMessage}</Text> : null}

      <TouchableOpacity style={styles.share} onPress={onShare}>
        <Text style={styles.btnText}>Share</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate('HomeContainer')
        }}>
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </View>
  )
}

export default ShareScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  share: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#5856d6',
    borderRadius: 5,
    justifyContent: 'center',
  },
  input: {
    marginTop: 20,
    width: 200,
    height: 50,
    padding: 10,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#000',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  text: {
    textAlign: 'center',
    fontSize: 24,
  },
  scrollWidth: {
    width: '100%',
  },
})
