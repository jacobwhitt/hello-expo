import React from 'react'
import { TouchableOpacity, StyleSheet, Text, View, Image } from "react-native";

const IosImage = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate("iOS Welcome");
        }}
      >
        <Text style={styles.btnText}>Back</Text>
      </TouchableOpacity>

      <View style={styles.imageContainer}>
        <Text style={styles.title}>A secret image appears!</Text>
        <Image
          style={styles.image}
          source={require("../../assets/ios14.png")}
        />
      </View>
    </View>
  );
};

export default IosImage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  back: {
    position: "absolute",
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: "#9a031e",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 2,
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    fontStyle: "italic",
  },
  image: {
    width: 200,
    height: 200,
  },
  imageContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 24,
  },
  title: {
    fontSize: 30,
    marginBottom: 20,
  },
});
