import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

const iOSComponent = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate("PlatformCheck");
        }}
      >
        <Text style={styles.btnText}>Back</Text>
      </TouchableOpacity>

      <View style={styles.welcome}>
        <Text style={styles.title}>Welcome to the secret iOS component!</Text>

        <Text style={styles.subtitle}>
          Swipe right from the left screen edge to open the drawer navigator for
          secret nested routes!
        </Text>
      </View>
    </View>
  );
};

export default iOSComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  back: {
    position: "absolute",
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: "#9a031e",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 2,
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    fontStyle: "italic",
  },
  text: {
    fontSize: 24,
    textAlign: "center",
  },
  title: {
    fontSize: 30,
    textAlign: "center",
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 20,
    marginTop: 20,
    textAlign: "center",
  },
  welcome: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});
