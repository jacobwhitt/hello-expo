import React, { useState } from 'react'
import { Pressable, LayoutAnimation, Platform, StyleSheet, Text, TouchableOpacity, UIManager, View } from 'react-native'

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true)
}
const ExpandScreen = (props) => {
  const [expanded, setExpanded] = useState(false)
  const [expanded2, setExpanded2] = useState(false)
  const [timesPressed, setTimesPressed] = useState(0)

  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 24, marginBottom: 40 }}>Pressable + Animation + Expand</Text>
      <Pressable
        onLongPress={() => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
          setExpanded(!expanded)
        }}
        delayLongPress={1000}
        hitSlop={20}
        pressRetentionOffset={{ bottom: 10, left: 10, right: 10, top: 10 }}
        style={({ pressed }) => [{ backgroundColor: pressed ? '#48bfe3' : '#5390d9' }, styles.wrapperCustom]}>
        {({ pressed }) => <Text style={styles.text}>{pressed ? 'Animating!' : 'Long Press Here!'}</Text>}
      </Pressable>

      {expanded && (
        <View style={styles.tile}>
          <Text style={styles.text}>I disappear sometimes!</Text>
        </View>
      )}

      <Pressable
        onPress={() => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
          setExpanded2(!expanded2)
        }}
        hitSlop={20}
        pressRetentionOffset={{ bottom: 10, left: 10, right: 10, top: 10 }}
        style={({ pressed }) => [{ backgroundColor: pressed ? '#48bfe3' : '#5390d9' }, styles.wrapperCustom]}>
        {({ pressed }) => <Text style={styles.text}>{pressed ? 'Animating!' : 'onPress'}</Text>}
      </Pressable>

      {expanded2 && (
        <View style={styles.tile}>
          <Text style={styles.text}>I disappear sometimes!</Text>
        </View>
      )}

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate('HomeContainer')
        }}>
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </View>
  )
}

export default ExpandScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  tile: {
    backgroundColor: '#6910c3',
    marginTop: 20,
    padding: 10,
    borderRadius: 10,
  },
  text: {
    color: '#fff',
  },
  wrapperCustom: {
    marginTop: 20,
    borderRadius: 10,
    padding: 10,
  },
  btnText: {
    fontSize: 20,
    color: '#FFFFFF',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
