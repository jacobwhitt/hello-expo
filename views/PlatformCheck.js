import React, { useState } from "react";
import {
  Platform,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
} from "react-native";

const PlatformCheck = (props) => {
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate("HomeContainer");
        }}
      >
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>

      <View style={styles.body}>
        {Platform.OS === "ios" ? (
          <Text style={styles.text}>Hey, you're using iOS!</Text>
        ) : (
          <Text style={styles.text}>Hey, you're using Android!</Text>
        )}

        <Text style={[styles.text, { marginTop: 10, marginBottom: 10 }]}>
          You can control everything to dynamically render content based on the
          mobile platform (OS) being used
        </Text>
        <Text style={styles.text1}>Color changed by Platform</Text>
        <Text style={styles.text2}>Font Size changed by Platform</Text>
        <Text style={styles.text3}>
          Line Height & Font Size changed by Platform
        </Text>

        <Text style={{ color: "#000" }}>No dynamic styling T___T</Text>
      </View>

      <Image style={styles.image} source={require("../assets/friends.png")} />

      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          {
            Platform.OS === "ios"
              ? props.navigation.navigate("IosComponent")
              : props.navigation.navigate("AndroidComponent");
          }
        }}
      >
        <Text style={styles.btnText}>Secret Page!</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default PlatformCheck;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  back: {
    position: "absolute",
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: "#9a031e",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  body: {
    marginTop: Platform.OS != 'ios' ? 50 : null,
  },
  button: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: "#6aa40f",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: Platform.OS === "ios" ? 200 : 200,
    height: Platform.OS === "ios" ? 200 : 200,
    margin: 20,
  },
  btnText: {
    fontSize: 24,
    color: "#fff",
  },
  text: {
    fontSize: 24,
  },
  text1: {
    fontSize: 24,
    color: Platform.OS === "ios" ? "dodgerblue" : "#a4c639",
  },
  text2: {
    fontSize: Platform.OS === "ios" ? 36 : 18,
  },
  text3: {
    fontSize: 24,
    fontSize: Platform.OS === "ios" ? 18 : 20,
    lineHeight: Platform.OS === "ios" ? 50 : 30,
  },
});
