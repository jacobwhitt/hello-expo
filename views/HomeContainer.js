import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const HomeContainer = (props) => {
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Welcome to the App!</Text>
      <Text style={[styles.text, { width: '80%' }]}>Swipe from the left side of the screen to open the drawer menu</Text>

      <View style={styles.grid}>
        <TouchableOpacity
          style={styles.redBtn}
          onPress={() => {
            props.navigation.navigate('Contacts')
          }}>
          <Text style={styles.btnText}>Contacts</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.orangeBtn}
          onPress={() => {
            props.navigation.navigate('PlatformCheck')
          }}>
          <Text style={styles.btnText}>Platform Check</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.lightOrangeBtn}
          onPress={() => {
            props.navigation.navigate('Pressable')
          }}>
          <Text style={styles.btnText}>Pressable</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.yellowBtn}
          onPress={() => {
            props.navigation.navigate('Modal')
          }}>
          <Text style={styles.btnText}>Modal</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.greenBtn}
          onPress={() => {
            props.navigation.navigate('Refresh')
          }}>
          <Text style={[styles.btnText, { fontSize: 18 }]}>Refresh Control</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tealBtn}
          onPress={() => {
            props.navigation.navigate('Expand')
          }}>
          <Text style={[styles.btnText, { fontSize: 18 }]}>Expand Animation</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.indigoBtn}
          onPress={() => {
            props.navigation.navigate('Share')
          }}>
          <Text style={styles.btnText}>Share API</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.redBtn}
          onPress={() => {
            props.navigation.navigate('StaticImage')
          }}>
          <Text style={styles.btnText}>Static Image</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.orangeBtn}
          onPress={() => {
            props.navigation.navigate('Pixel')
          }}>
          <Text style={styles.btnText}>Pixel Ratio</Text>
        </TouchableOpacity>

        {/* <TouchableOpacity
          style={styles.lightOrangeBtn}
          onPress={() => {
            props.navigation.navigate('Orientation')
          }}>
          <Text style={styles.btnText}>Orientation</Text>
        </TouchableOpacity> */}

        <TouchableOpacity
          style={styles.lightOrangeBtn}
          onPress={() => {
            props.navigation.navigate('Battery')
          }}>
          <Text style={styles.btnText}>Battery API</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.yellowBtn}
          onPress={() => {
            props.navigation.navigate('ImagePicker')
          }}>
          <Text style={styles.btnText}>ImagePicker API</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.greenBtn}
          onPress={() => {
            props.navigation.navigate('Notifications')
          }}>
          <Text style={styles.btnText}>Notifications API</Text>
        </TouchableOpacity>
      </View>

      {/* <TouchableOpacity
        style={styles.logout}
        onPress={() => {
          props.navigation.navigate('Login')
        }}>
        <Text style={styles.btnText}>Logout</Text>
      </TouchableOpacity> */}
    </SafeAreaView>
  )
}

export default HomeContainer

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  grid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  title: {
    fontSize: 30,
    marginTop: 20,
  },
  subtitle: {
    fontSize: 20,
    marginTop: 20,
    textAlign: 'center',
  },
  text: {
    fontSize: 18,
    marginTop: 20,
    textAlign: 'center',
  },
  redBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#f94144',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  orangeBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#f3722c',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  lightOrangeBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: '#f8961e',
    alignItems: 'center',
    justifyContent: 'center',
  },
  yellowBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: '#f9c74f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  greenBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#90be6d',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tealBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#43aa8b',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indigoBtn: {
    width: 140,
    height: 50,
    marginTop: 20,
    backgroundColor: '#577590',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  logout: {
    position: 'absolute',
    top: 30,
    left: 20,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
