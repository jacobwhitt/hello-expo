import React, { useState } from 'react'
import { Dimensions, Image, PixelRatio, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import normalize from 'react-native-normalize'

const size = 100
const image = {
  uri: 'http://www.revista-gadget.es/wp-content/uploads/2019/06/53670377_2223047137752807_7850138892014452736_o-1900x700_c.jpg',
  width: size,
  height: size,
}
const { width, height } = Dimensions.get('window')

//* PixelRatio gives you access to the device's pixel density and font scale.
//? Multiply the size of the image you display by the pixel ratio

const PixelRatioDemo = (props) => {
  const [type, setType] = useState(true)

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      {type ? (
        <View>
          <View style={styles.container}>
            <Text>Current Pixel Ratio is:</Text>
            <Text style={styles.text}>{PixelRatio.get()}</Text>
          </View>

          <View style={styles.container}>
            <Text>Current Font Scale is:</Text>
            <Text style={styles.text}>{PixelRatio.getFontScale()}</Text>
          </View>

          <View style={styles.container}>
            <Text>On this device images with a layout width of</Text>
            <Text style={styles.text}>{size} px</Text>
            <Image source={image} />
          </View>

          <View style={styles.container}>
            <Text>require images with a pixel width of</Text>

            <Text style={styles.text}>{PixelRatio.getPixelSizeForLayoutSize(size)} px</Text>

            <Image
              source={image}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(size),
                height: PixelRatio.getPixelSizeForLayoutSize(size),
              }}
            />
          </View>
        </View>
      ) : (
        <View style={styles.normalize}>
          <View style={styles.caja}>
            <Text style={styles.normalizedText}>React Native Normalize</Text>
            <Text style={styles.normalizedText}>This is some text</Text>
            <Text style={styles.normalizedText}>Más Texto Aún</Text>

            <Image style={styles.normalImage} source={image} />
          </View>
        </View>
      )}

      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate('HomeContainer')
        }}>
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.typeBtn}
        onPress={() => {
          setType(!type)
        }}>
        <Text style={styles.btnText}>{type ? 'React-Normalize' : 'Pixel Ratio'}</Text>
      </TouchableOpacity>
    </ScrollView>
  )
}

export default PixelRatioDemo

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    marginTop: 20,
    justifyContent: 'center',
  },
  container: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 24,
    textAlign: 'center',
  },
  back: {
    position: 'absolute',
    top: 0,
    left: 20,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  typeBtn: {
    position: 'absolute',
    top: 0,
    right: 20,
    width: 100,
    height: 40,
    marginTop: 20,
    backgroundColor: '#90be6d',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  normalize: {
    width: '100%',
    height: normalize(300, 'height'),
  },
  caja: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  normalizedText: {
    textAlign: 'center',
    fontSize: normalize(36),
    lineHeight: normalize(60),
  },
  normalImage: {
    width: normalize(100),
    height: normalize(100, 'height'),
  },
})
