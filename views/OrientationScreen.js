import React, { useEffect, useState } from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, ScrollView, SafeAreaView, View } from 'react-native'

const { width, height } = Dimensions.get('screen')
console.log('width', width)
console.log('height', height)

const OrientationScreen = (props) => {
  const [orientation, setOrientation] = useState('')
  const [layout, setLayout] = useState({})

  useEffect(() => {
    let mounted = true
    if (mounted) {
      setOrientation(isPortrait() ? 'Portrait' : 'Landscape')
    }
    return () => (mounted = false)
  }, [])

  Dimensions.addEventListener('change', () => {
    setOrientation(isPortrait() ? 'Portrait' : 'Landscape')
  })

  const isPortrait = () => {
    const dim = Dimensions.get('screen')
    return dim.height >= dim.width
  }

  const onLayoutChange = (event) => {
    console.log('----------', JSON.stringify(event.nativeEvent.layout))
    setLayout({ height: event.nativeEvent.layout.height, width: event.nativeEvent.layout.width })
  }

  return (
    <View style={styles.container}>
      {orientation === 'Portrait' ? (
        <View style={portraitStyles.container}>
          <View style={styles.textContainer}>
            <Text style={styles.btnText}>Portrait</Text>
          </View>
        </View>
      ) : (
        <View style={[landscapeStyles.container, { width: layout.width, height: layout.height }]}>
          <View style={styles.textContainer}>
            <Text style={styles.btnText}>Landscape</Text>
          </View>
        </View>
      )}
      <View onLayout={onLayoutChange} />
    </View>
  )
}

export default OrientationScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#999',
  },
  textContainer: {
    width: 180,
    height: 50,
    backgroundColor: '#43aa8b',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
})

const portraitStyles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'plum',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
})

const landscapeStyles = StyleSheet.create({
  container: {
    transform: [{ rotate: '90deg' }],
    flex: 1,
    // width: width,
    // height: height,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
})
