import React, { useState, useEffect } from 'react'
import { Dimensions, RefreshControl, SafeAreaView, StyleSheet, Text, View, FlatList, Image, TouchableOpacity } from 'react-native'
import Item from '../components/other/Item'

import { CONTACTS } from '../mocks/data/contacts.mock'
const height = Dimensions.get('window').height

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout)
  })
}

const ContactsScreen = (props, route) => {
  const [selectedId, setSelectedId] = useState(null)
  const [index, setIndex] = useState(0)
  const [refreshing, setRefreshing] = useState(false)

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)

    wait(2000).then(() => {
      setRefreshing(false)
      setSelectedId(null)
    })
  }, [])

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? '#482211' : '#028090'

    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id)
          setIndex(item.index)
          props.navigation.navigate('Details', { id: item.id, name: item.name, surname: item.surname, email: item.email, phone: item.phone })
        }}
        style={[styles.item, { backgroundColor: backgroundColor }]}
      />
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate("HomeContainer");
        }}
      >
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>

      <View style={styles.containerTop}>
        <FlatList
          contentContainerStyle={styles.flatlist}
          data={CONTACTS}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
          numColumns={1}
          getItemLayout={(data, index) => ({
            length: 50,
            offset: 50 * index,
            index,
          })}
          initialScrollIndex={0}
          refreshControl={
            <RefreshControl
              title="How refreshing!"
              titleColor="#000"
              tintColor="#000"
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }
        />
      </View>

      <View style={styles.containerBottom}>
        {selectedId ? (
          <Text style={styles.text}>
            Has seleccionado:
            {CONTACTS.filter((contact) => contact.id === selectedId).map(
              (contact) => contact.name
            )}
          </Text>
        ) : (
          <Text style={styles.text}>
            Scroll in the list and press a User Card
          </Text>
        )}
      </View>
    </SafeAreaView>
  );
}

export default ContactsScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    alignItems: "center",
    justifyContent: "center",
  },
  back: {
    position: "absolute",
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: "#9a031e",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    fontStyle: "italic",
  },
  containerTop: {
    marginTop: Platform.OS === 'ios' ? 40 : 100,
    width: "100%",
    flex: 3,
  },
  containerBottom: {
    flex: 0.2,
    marginTop: 10,
    borderTopWidth: 1,
    borderTopColor: "#ccc",
    justifyContent: "center",
    alignItems: "center",
  },
  flatlist: {
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 20,
  },
});
