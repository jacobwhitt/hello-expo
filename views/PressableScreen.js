import React, { useState } from 'react'
import {
  Modal,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

const PressableScreen = (props) => {
  const [timesPressed, setTimesPressed] = useState(0);
  const [showModal, setShowModal] = useState(false);

  let textLog = "Times pressed appears here";
  if (timesPressed > 1) {
    textLog = timesPressed + "x onPress";
  } else if (timesPressed > 0) {
    textLog = "1 onPress";
  }

  return (
    <View style={styles.container}>
      <Pressable
        onPress={() => {
          setTimesPressed((current) => current + 1);
        }}
        delayLongPress={1000}
        onLongPress={() => {
          setShowModal(true);
          setTimesPressed(0);
        }}
        style={({ pressed }) => [
          { backgroundColor: pressed ? "rgb(210, 230, 255)" : "white" },
          styles.wrapperCustom,
        ]}
      >
        {({ pressed }) => (
          <Text style={styles.text}>{pressed ? "Pressed!" : "Press Me"}</Text>
        )}
      </Pressable>

      <Text style={[styles.text, { marginTop: 10 }]}>
        Try a long press, too!
      </Text>
      <View style={styles.logBox}>
        <Text>{textLog}</Text>
      </View>

      <View>
        <Modal animationType="slide" transparent={true} visible={showModal}>
          <View style={styles.modalContainer}>
            <View style={styles.modal}>
              <Text style={styles.modalText}>Hello World!</Text>

              <TouchableOpacity
                style={{ ...styles.openButton, backgroundColor: "#21a6a1" }}
                onPress={() => {
                  setShowModal(!showModal);
                }}
              >
                <Text style={styles.textStyle}>Hide Modal</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
      <TouchableOpacity
        style={styles.back}
        onPress={() => {
          props.navigation.navigate("HomeContainer");
        }}
      >
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PressableScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  back: {
    position: "absolute",
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: "#9a031e",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    fontStyle: "italic",
  },
  flexBox: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  logBox: {
    padding: 20,
    margin: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#ccc",
    backgroundColor: "#fff",
  },
  modal: {
    margin: 40,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 55,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalContainer: {
    top: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  text: {
    fontSize: 16,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  wrapperCustom: {
    borderRadius: 8,
    padding: 6,
  },
});
