import React, { Component } from 'react'
import * as Battery from 'expo-battery'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { FontAwesome, FontAwesome5, Ionicons } from '@expo/vector-icons'

export default class BatteryDemo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      level: null,
      state: null,
      lowPowerMode: null,
      color: null,
    }
  }

  componentDidMount() {
    this._subscribe()
  }

  componentWillUnmount() {
    this._unsubscribe()
  }

  batteryColor = (batteryLevel, lowPowerMode) => {
    if (lowPowerMode) {
      //? LOW POWER = YELLOW
      return '#fecb2e'
    } else if (batteryLevel > 0.6) {
      //* OVER 60% = GREEN
      return '#53d769'
    } else if (batteryLevel < 0.6 && batteryLevel > 0.2) {
      //TODO 20-60% = ORANGE
      return '#fd9426'
    } else {
      //! UNDER 20% = RED
      return '#fc3d39'
    }
  }

  roundNum = (num, places) => {
    return +((Math.round(num + 'e+' + places) * 100) / 100)
  }

  async _subscribe() {
    const { batteryLevel, batteryState, lowPowerMode } = await Battery.getPowerStateAsync()

    this.setState({ level: this.roundNum(batteryLevel, 2), state: batteryState, lowPowerMode: lowPowerMode, color: this.batteryColor(batteryLevel, lowPowerMode) })
    console.log('state: ', this.state)

    this._subscription = Battery.addBatteryLevelListener(({ batteryLevel }) => {
      this.setState({ level: this.roundNum(batteryLevel, 2) })
      console.log('batteryLevel changed!', batteryLevel)
    })

    this._subscription = Battery.addBatteryStateListener(({ batteryState }) => {
      this.setState({ state: batteryState })
      console.log('batteryState changed!', batteryState)
    })

    this._subscription = Battery.addLowPowerModeListener(({ lowPowerMode }) => {
      this.setState({ lowPowerMode: lowPowerMode })
      console.log('batteryState changed!', lowPowerMode)
    })
  }

  _unsubscribe() {
    this._subscription && this._subscription.remove()
    this._subscription = null
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Current Battery Level: {this.state.level} %</Text>

        {this.state.state === 2 ? <Text style={styles.text}>Phone is currently charging</Text> : <Text style={styles.text}>Phone is running off battery power</Text>}

        <Text style={styles.text}>Low Power Mode: {this.state.lowPowerMode ? <Text style={styles.text}>On</Text> : <Text style={styles.text}>Off</Text>}</Text>

        <View style={{ height: 200, flexDirection: 'row' }}>
          <View style={styles.batteryContainer}>
            <View
              style={[
                {
                  flex: 1,
                  width: this.state.level < 95 ? `${this.state.level}%` : '98%',
                  backgroundColor: this.state.color,
                  borderTopRightRadius: `${this.state.level}` > 90 ? 5 : `${this.state.level}` > 95 ? 25 : 0,
                  borderBottomRightRadius: `${this.state.level}` > 90 ? 5 : `${this.state.level}` > 95 ? 25 : 0,
                },
                styles.batteryColor,
              ]}></View>
            <Text style={styles.charging}>{this.state.state === 2 ? <FontAwesome name='bolt' size={48} color='black' /> : null}</Text>
          </View>
          <View style={styles.batteryTip}></View>
        </View>

        <TouchableOpacity
          style={styles.back}
          onPress={() => {
            this.props.navigation.navigate('HomeContainer')
          }}>
          <Text style={styles.btnText}>Home</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  batteryContainer: {
    width: 200,
    height: 80,
    marginTop: 20,
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 10,
    // alignItems: 'center',
  },
  batteryTip: {
    position: 'relative',
    top: 45,
    width: 6,
    height: 30,
    borderWidth: 2,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderRightColor: '#000',
    borderLeftColor: 'transparent',
  },
  batteryColor: {
    marginVertical: 2,
    marginHorizontal: 2,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  charging: {
    position: 'absolute',
    top: '25%',
    width: 200,
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  subtitle: {
    fontSize: 20,
    textAlign: 'center',
  },
  text: {
    fontSize: 18,
    marginTop: 20,
    textAlign: 'center',
  },
  back: {
    position: 'absolute',
    left: 20,
    top: 30,
    width: 80,
    height: 40,
    marginTop: 20,
    backgroundColor: '#9a031e',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
  },
})
